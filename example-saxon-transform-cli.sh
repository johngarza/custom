#!/bin/sh
#customize for your current report
XSL=Custom.xsl
INPUT_XML="custom.xml"
OUTPUT_HTML="Custom.html"
SAXON_HOME=~/bin/saxon/saxon9.jar
SESSION_ID="your_session_id_goes_here"
BASE_URL="http://your_r25ws_server_goes_here/r25ws/wrd/run/"

java -jar ${SAXON_HOME} -s:${INPUT_XML} -xsl:${XSL} -o:${OUTPUT_HTML} base_url=${BASE_URL} session_id=${SESSION_ID}
