# Customizing R25WS Reports #

Here is the example code from my 2014 presentation at the CollegeNET UserConference.  Enjoy.  There are branches for each example in the presentation powerpoint.

### Examples ###

* simple-1: A basic report pulling data from secgroup.xml, watch out, the security group id is hardcoded!
* intermediate-1: Augmenting simple-1 to also display each users's reservations for the past week.
* intermediate-2: Using a custom function as well as r25fn functions to make the report easier to read (both in source and in the final output)
* advanced-1: We combine simple-1's secgroup.xml request with audit.xml to create a summary of each users's workload for the month of May 2014

### What's next? ###

* take these examples and make them work for any installation by removing any hardcoded report parameters
* test with FOP/Prince to make sure any PDF pipelines still look ok

### License ###

* MIT
