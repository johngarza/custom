<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xl="http://www.w3.org/1999/xlink"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:r25="http://www.collegenet.com/r25"
	xmlns:r25fn="http://www.collegenet.com/r25/functions"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="#all"
	version="2.0">
	
<xsl:import href="../../../common/report-styles.xsl" />
<xsl:import href="../../../common/common.xsl" />
	
<xsl:output method="xml" encoding="UTF-8" indent="no" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
	
<xsl:param name="base_url" select="''" />
<xsl:param name="session_id" select="''" />
<xsl:param name="lang" select="'en'" />
<xsl:param name="country" select="'US'" />
<xsl:param name="timezone" />
	
<xsl:variable name="REPORT_FILENAME" select="reverse(tokenize(replace(static-base-uri(),'/','\\'),'\\'))[1]"/>
<xsl:variable name="REPORT_TITLE" select="/r25:reports/r25:report/r25:report_name"/>
	
<!--Retrieve all R25 users for a specific security group -->
<!-- secgroup.xml?security_group_id=33&scope=extended -->
<xsl:variable name="r25users_for_group_url" select="concat($base_url, 'secgroup.xml?scope=extended&amp;security_group_id=33&amp;session_id=',$session_id)" />
<xsl:variable name="r25usersnames" select="document($r25users_for_group_url)/r25:security_groups/r25:security_group/r25:security_group_members" />

<xsl:template match="/">
	<html><xsl:attribute name="xml:lang" select="$lang"/>
	<head>

	<style type="text/css" media="print">
		<xsl:call-template name="CommonStyles"/>
		/* Report specific styles */
		@page report {
			size: portrait;
		}
	</style>

	</head>
<body class="Local">
	<xsl:attribute name="id" select="$REPORT"/>
	<div class="Header">
		<div class="TanHeaderLand"/>
		<div class="ReportTitleWhite"><xsl:value-of select="$REPORT_TITLE"/></div>
	</div>
	<div class="Data">
		<xsl:choose>
			<xsl:when test="count($r25usersnames) = 0">
				<p class="NoData"><xsl:value-of select="r25fn:text('No contacts found')"/></p>
			</xsl:when>
			<xsl:otherwise>
				<table><col width="1*"/><col width="4*"/>
					<thead>					
						<tr class="ColumnHeaders">
							<th><xsl:value-of select="r25fn:text('Contact')"/></th>
							<th><xsl:value-of select="r25fn:text('Workload - May 2014')"/></th>
						</tr>
						<tr class="PadTop"><td/></tr>
					</thead>
					<tbody>
						<xsl:choose>
							<xsl:when test="count($r25usersnames) = 0">
								<tr>
									<td colspan="2" class="NoData"><xsl:value-of select="r25fn:text('No users found')"/></td>
								</tr>
							</xsl:when>
							<xsl:otherwise>
								<xsl:apply-templates select="$r25usersnames">
									<xsl:sort select="r25:group_member_name" />
								</xsl:apply-templates>
							</xsl:otherwise>
						</xsl:choose>
					</tbody>
				</table>
			</xsl:otherwise>
		</xsl:choose>
	</div><!-- end Data -->
</body>
</html>
</xsl:template>

<xsl:template match="r25:security_group_members">
	<xsl:variable name="CurUser" select="." />
	<xsl:variable name="CurUserName" select="$CurUser/r25:group_member_username" />
	<xsl:variable name="AuditTypes" select="'&amp;object_type=1&amp;audit_type=1001+1100+1101+1110+1111+1+2'" />
	<xsl:variable name="DateRange" select="'&amp;start_dt=20140501T0000&amp;end_dt=20140531T0000'" />
	<xsl:variable name="Session" select="concat('&amp;session_id=',$session_id)" />
	<xsl:variable name="audit_data_url"
		select="concat($base_url,'audit.xml?audit_user=',$CurUserName,$AuditTypes,$DateRange,$Session)" />
	<xsl:variable name="AuditData" select="document($audit_data_url)" />	
	<xsl:variable name="CurUserAuditData" as="item()*">
		<audit_data>
				<xsl:attribute name="creates" select="count($AuditData/r25:audit/r25:audit_item[r25:type = 1])" />
				<xsl:attribute name="modifications" select="count($AuditData/r25:audit/r25:audit_item[r25:type = 2])" />
				<xsl:attribute name="states" select="count($AuditData/r25:audit/r25:audit_item[r25:type = 1001])" />
				<xsl:attribute name="sp_assigns" select="count($AuditData/r25:audit/r25:audit_item[r25:type = 1100])" />
				<xsl:attribute name="rs_assigns" select="count($AuditData/r25:audit/r25:audit_item[r25:type = 1101])" />
				<xsl:attribute name="sp_removes" select="count($AuditData/r25:audit/r25:audit_item[r25:type = 1110])" />
				<xsl:attribute name="rs_removes" select="count($AuditData/r25:audit/r25:audit_item[r25:type = 1111])" />
		</audit_data>
	</xsl:variable>
	<tr>
		<td><xsl:value-of select="r25:group_member/r25:group_member_name"/></td>
		<td><table><col width="2*"/><col width="2*"/><col width="2*"/><col width="2*"/><col width="2*"/><col width="2*"/><col width="2*"/>
			<thead>
			<tr class="ColumnHeaders">
						<th><xsl:value-of select="r25fn:text('New Events')"/></th>
						<th><xsl:value-of select="r25fn:text('Modifications')"/></th>
						<th><xsl:value-of select="r25fn:text('State Changes')"/></th>
						<th><xsl:value-of select="r25fn:text('Space Assignments')"/></th>
						<th><xsl:value-of select="r25fn:text('Resource Assignments')"/></th>
						<th><xsl:value-of select="r25fn:text('Space Removals')"/></th>
						<th><xsl:value-of select="r25fn:text('Resource Removals')"/></th>
			</tr>
			</thead>
			<tbody><tr>
				<xsl:call-template name="OutputItem">
					<xsl:with-param name="AuditData" select="$CurUserAuditData" />
				</xsl:call-template>
			</tr></tbody>
		</table></td>
	</tr>
</xsl:template>

<xsl:template name="OutputItem">
	<xsl:param name="AuditData" />
	<td><xsl:value-of select="$AuditData/@creates" /> </td>
	<td><xsl:value-of select="$AuditData/@modifications" /> </td>
	<td><xsl:value-of select="$AuditData/@states" /> </td>
	<td><xsl:value-of select="$AuditData/@sp_assigns" /> </td>
	<td><xsl:value-of select="$AuditData/@rs_assigns" /> </td>
	<td><xsl:value-of select="$AuditData/@sp_removes" /> </td>
	<td><xsl:value-of select="$AuditData/@rs_removes" /> </td>
</xsl:template>

</xsl:stylesheet>
